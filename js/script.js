document
  .getElementById("find-ip-button")
  .addEventListener("click", async () => {
    try {
      const ipResponse = await fetch("https://api.ipify.org/?format=json");
      const ipData = await ipResponse.json();
      const ip = ipData.ip;

      const locationResponse = await fetch(
        `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
      );
      const locationData = await locationResponse.json();

      const ipInfoDiv = document.getElementById("ip-info");
      ipInfoDiv.innerHTML = `
          <p>Континент: ${locationData.continent}</p>
          <p>Країна: ${locationData.country}</p>
          <p>Регіон: ${locationData.regionName}</p>
          <p>Місто: ${locationData.city}</p>
          <p>Район: ${locationData.district || ""}</p>
      `;
    } catch (error) {
      console.error("Помилка при отриманні інформації про IP:", error);
    }
  });
